#!/bin/bash

set -x

# Collect

mkdir data

cd data

wget -c https://datahub.io/core/population/r/population.csv

# Clean
grep 'Spain' population.csv | cut -d',' -f3,4 | sed 's/,/\t/g' > data_spain.tsv

# Visualize
gnuplot -e "set terminal dumb; plot 'data_spain.tsv' using 1:2"




